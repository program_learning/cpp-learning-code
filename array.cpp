#include <iostream>
using namespace std;

void arrOut(int arr[], int len);

int main()
{
    /* 一维数组定义的三种方式: 
     * 1. 数据类型 数组名[数组长度];
     * 2. 数据类型 数组名[数组长度] = {值1，值2...}; 
     * 3. 数据类型 数组名[] = {值1，值2 ...}; (自动判断数组长度)
     */

    // No.1
    int score[3];
    score[0] = 1;
    score[1] = 2;
    score[2] = 3;
    arrOut(score, 3);

    return 0;
}

void arrOut(int arr[], int len)
{
    for (int i = 0; i < len; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
}